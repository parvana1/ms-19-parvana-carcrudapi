package az.ingress.carCrudApi.repository;

import az.ingress.carCrudApi.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car,Integer> {
}
