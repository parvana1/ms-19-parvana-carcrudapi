package az.ingress.carCrudApi.service.impl;


import az.ingress.carCrudApi.model.Car;
import az.ingress.carCrudApi.repository.CarRepository;
import az.ingress.carCrudApi.service.CarService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    public CarServiceImpl(CarRepository carRepository){
        this.carRepository=carRepository;
    }
    @Override
    public Car getCarById(Integer id) {
        Optional<Car> car=carRepository.findById(id);
        if(car.isEmpty()){
            throw new RuntimeException();
        }
        return car.get();
    }

    @Override
    public List<Car> getAllCars() {
      return   carRepository.findAll();

    }

    @Override
    public Car createCar(Car car) {
        return carRepository.save(car);
    }

    @Override
    public Car updateCar(Integer id, Car car) {
        Car carData=carRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Car not found"));
        carData.setModel(car.getModel());
        carData.setPrice(car.getPrice());
        carData.setYears(car.getYears());
        carData.setColor(car.getColor());
        carData.setCondition(car.getCondition());

        carData=carRepository.save(carData);
        return carData;

    }

    @Override
    public void deleteCar(Integer id) {
        carRepository.deleteById(id);

    }
}
