package az.ingress.carCrudApi.service;

import az.ingress.carCrudApi.model.Car;

import java.util.List;


public interface CarService {
    Car getCarById(Integer id);
    List<Car> getAllCars();
    Car createCar(Car car);
    Car updateCar(Integer id,Car car);
    void  deleteCar(Integer id);

}
