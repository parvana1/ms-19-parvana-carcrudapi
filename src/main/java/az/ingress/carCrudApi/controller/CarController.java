package az.ingress.carCrudApi.controller;

import az.ingress.carCrudApi.model.Car;
import az.ingress.carCrudApi.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/car")
public class CarController {
 private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }


    @GetMapping("/{id}")
    public Car get(@PathVariable Integer id){

      return  carService.getCarById(id);
    }
    @GetMapping("/getAllCars")
    public List<Car> get(){
        return  carService.getAllCars();
    }

    @PostMapping
    public Car create(@RequestBody Car car){
        return  carService.createCar(car);
    }
    @PutMapping
    public Car update( @PathVariable Integer id,@RequestBody Car car){
       return carService.updateCar(id,car);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id){
        carService.deleteCar(id);
    }

}
