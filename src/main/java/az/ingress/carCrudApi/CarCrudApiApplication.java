package az.ingress.carCrudApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarCrudApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarCrudApiApplication.class, args);
	}

}
